from fastapi import FastAPI, Request, HTTPException
from elg.model import TextRequest
from elg import Service, Authentication
from dotenv import load_dotenv
from os import environ
import time
import json

from typing import Tuple

from loguru import logger

load_dotenv()

app = FastAPI()

with open("./languages.json") as f:
    languages = json.load(f)
models = [model for src in languages["src"] for tgt in src["tgt"] for model in tgt["models"]]

CHAR_LIMIT = int(environ.get("CHAR_LIMIT"))

elg_execution_location_sync = "https://live.european-language-grid.eu/execution/process/{accessor_id}"
elg_execution_location = "https://live.european-language-grid.eu/async/execution/process/{accessor_id}"

authentication = Authentication("live")
authentication.access_token = "xxx"
authentication.expires_time = time.gmtime()
authentication.refresh_expires_time = time.gmtime()
authentication.refresh_token = environ.get("REFRESH_TOKEN").replace("\n", "")
authentication.token_type = "bearer"
authentication.id_token = None
authentication.scope = "ELG-profile profile email offline_access"

try:
    authentication.refresh()
except:
    logger.error(f"Impossible to refresh the token: {authentication.refresh_token}")

record = {
    "service_info": {
        "elg_execution_location_sync": elg_execution_location_sync.format(accessor_id="xxx"),
        "elg_execution_location": elg_execution_location.format(accessor_id="xxx"),
    }
}

universal_service = Service(
    **{
        "id": 0,
        "resource_name": None,
        "resource_short_name": None,
        "resource_type": "ToolService",
        "entity_type": None,
        "description": None,
        "keywords": None,
        "detail": None,
        "licences": None,
        "languages": None,
        "country_of_registration": None,
        "creation_date": None,
        "last_date_updated": None,
        "functional_service": None,
        "functions": None,
        "intended_applications": None,
        "views": None,
        "downloads": None,
        "size": None,
        "service_execution_count": None,
        "status": None,
        "under_construction": None,
        "record": record,
        "auth_object": authentication,
        "auth_file": None,
        "scope": None,
        "domain": "https://live.european-language-grid.eu",
        "use_cache": None,
        "cache_dir": None,
    }
)

@app.post("/process/{accessor_id}")
async def process(accessor_id: str, request: Request):
    logger.info("{header}", header=request.headers)
    if accessor_id not in models:
        raise HTTPException(status_code=403, detail=f"The service [{accessor_id}] is not part of the authorized machine translation services.") 
    request_content = await request.json()
    text_request = TextRequest(**request_content)
    if len(text_request.content) > CHAR_LIMIT:
        raise HTTPException(status_code=401, detail=f"Too many characters in the request: {len(text_request.content)}. Limit is: {CHAR_LIMIT}")
    universal_service.elg_execution_location = elg_execution_location.format(accessor_id=accessor_id)
    universal_service.elg_execution_location_sync = elg_execution_location_sync.format(accessor_id=accessor_id)
    try:
        response = universal_service(text_request, sync_mode=True, timeout=300, verbose=False, check_file=False, output_func="auto")
    except Exception as e:
        raise HTTPException(status_code=404, detail=f"Issue during the service call: {e}")
    return {"response": response}

@app.get("/languages")
async def lang():
    return languages