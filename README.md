# Experiment for the ELG backend for the Ukrainian backend

## Objective

> Summarising discussion on email and in calls so far - it's not reasonable to expect each individual end-user of this service to register in their own right with the ELG platform, so the final user-facing system will require some kind of intermediary "back end" that receives requests from the UI, maybe applies things like rate limits per source IP (if this is possible in the infrastructure we have available) and then dispatches the actual translation requests to the ELG endpoints using its own "service account" token.

> However we choose to implement this it should be small and efficient, fast to start up from cold (so auto-scaling remains responsive) but at the same time as secure as we can make it against DoS or other types of attack - this is not just a theoretical consideration, if this ends up being promoted heavily by the EC as a tool to help Ukrainian refugees then it becomes an obvious target for Russian cyber attacks...

## Implementation

This experiment uses [FastAPI](https://fastapi.tiangolo.com/) to create the server.

The server exposes an endpoint that forward the request to the ELG LT service execution server.
Before forwarding the request, the authentification token is added to it.
The response coming from the MT service is then forwarded back to the client.

## Usage

### Run the FastAPI server

```bash
# Clone the repo
git clone https://gitlab.com/airKlizz/elg-proxy-uk-mt
cd elg-proxy-uk-mt

# Add a valid ELG REFRESH_TOKEN to .env file
echo REFRESH_TOKEN=XXX > .env

# Add a CHAR_LIMIT value to .env file
echo CHAR_LIMIT=1024 > .env

# Create languages.json file
echo '{"src": [{"shortName": "en", "longName": "English", "tgt": [{"shortName": "uk", "longName": "Ukrainian", "models": ["opus-mt-eng-ukr"]}]}]}' > languages.json

# Install requirements
pip install -r requirements.txt

# Start the server
uvicorn app.main:app
```

### Call the server

In Python

```python
>>> import requests

>>> url = "http://localhost:8000/process/"
>>> response = requests.post(url+"opus-mt-eng-bos", json={"content": "This is a text in English"})
>>> print(response)
<Response [200]>
```
